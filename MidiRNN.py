#Home directory: ./main/
from io import open
import glob
import os

import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import torch.multiprocessing as mp

import math
import random
from random import randrange
import time
from datetime import datetime
import numpy as np
import random
import sys

import mido
from mido import MidiFile

import matplotlib.pyplot as plt

#TQDM for progress bar
from tqdm import tqdm, tqdm_notebook

#Custom Dataset class
from MidiDataset import MidiDataset


#Use GPU for training if possible
cuda = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(cuda)


#
#Recurrent neural network
#
class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()
        self.hidden_size = hidden_size

        self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)
        self.o2o = nn.Linear(hidden_size + output_size, output_size)
        self.dropout = nn.Dropout(0.1)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        input_combined = torch.cat((input, hidden), 1)
        hidden = self.i2h(input_combined)
        output = self.i2o(input_combined)
        output_combined = torch.cat((hidden, output), 1)
        output = self.o2o(output_combined)
        output = self.dropout(output)
        output = self.softmax(output)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, self.hidden_size).to(device=cuda)


