#Home directory: ./main/
from io import open
import glob
import os

import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import torch.multiprocessing as mp

import math
import random
from random import randrange
import time
from datetime import datetime
import numpy as np
import random
import sys

import mido
from mido import MidiFile

import matplotlib.pyplot as plt

#TQDM for progress bar
from tqdm import tqdm, tqdm_notebook

#Custom Dataset class
from MidiDataset import MidiDataset


class MidiTraining():

	def __init__(self, midi_dataset, learning_rate):

		#Use GPU for training if possible
		cuda = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
		print(cuda)

		#Load training data into dataloader
		self.midi_dataset = midi_dataset
		self.midi_dataloader = DataLoader(self.midi_dataset, batch_size=1, shuffle=True, num_workers=0)


		#RNN Training  Hyper Parameters
		self.learning_rate = learning_rate

		#Loss function used for neural net
		self.criterion = nn.NLLLoss()

		#Get amount of training samples
		self.n_samples = len(self.midi_dataloader)
	
		#Get date time-stamp for model file
		self.date = datetime.now().strftime("%m-%d-%Y-%H:%M:%S")

	#
	#Training function
	#
	def train(self, model, input_line_tensor, target_line_tensor):

		target_line_tensor.unsqueeze_(-1)
		hidden = model.module.initHidden() if type(model).__name__ == "DataParallel" else model.initHidden()

		model.zero_grad()

		loss = 0

		for i in range(len(input_line_tensor)):
			
			output, hidden = model(input_line_tensor[i], hidden)
			l = self.criterion(output, target_line_tensor[i])
			loss += l

		loss.backward()

		#optimizer.step()
		for p in model.parameters():
			p.data.add_(-self.learning_rate, p.grad.data)

		return output, loss.item() / input_line_tensor.size(0)


	#Function so that epoch can be called in multiprocessing
	def train_epochs(self, rnn, epochs_per_thread, all_losses, n_processes):

		#Get time of training start
		start = time.time()

		#Training Hyper params
		print_every = epochs_per_thread * n_processes * (self.n_samples / self.midi_dataloader.batch_size) * 0.05 
		plot_every = epochs_per_thread * n_processes * (self.n_samples / self.midi_dataloader.batch_size) * 0.005
    
		total_loss = 0 # Reset every plot_every iters

		#Iterate through epochs
		for epoch in range(epochs_per_thread):

			for i, batch in enumerate(tqdm(self.midi_dataloader)):
				
				#Unpack batch
				batch_input, batch_target = batch

				output, loss = self.train(rnn, *batch_input, *batch_target)
				total_loss += loss

				#Save neural network model in models folder
				torch.save(rnn, "./models/" + self.date + ".pt")

				#Add 1 to i so that loss at first iteration won't be 0
				i += 1

				'''if (epoch * i) % print_every == 0:
				print('%s (%d %d%%) %.4f' % (timeSince(start), i, i / len(midi_dataloader) * 100, loss))'''

				if ((epoch * self.n_samples) + i) % plot_every == 0:
					all_losses.append(total_loss / plot_every)
					total_loss = 0
