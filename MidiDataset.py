from io import open
import glob
import os

import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import torch.multiprocessing as mp

import math
import random
from random import randrange
import time
from datetime import datetime
import numpy as np
import random
import sys

import mido
from mido import MidiFile

import matplotlib.pyplot as plt

#TQDM for progress bar
from tqdm import tqdm

#Class for Midi Dataset
class MidiDataset(Dataset):
    
	#
	#Data preperation
	#
	n_notes = 128
		
	#Function to find files in directory
	def findFiles(self, path): return glob.glob(path)

	#Initialize data
	def __init__(self, data_dir, device):

		#Call find tracks with instrument function to get dictionary of piano rolls
		self.pr_data = self.findTracksWithInstrument(data_dir, -1)
	
		#Get the number of unique sequences
		#Make unique_seqs a public attribute
		self.unique_seqs = self.uniqueSequences(self.pr_data)
		self.n_unique_seqs = len(self.unique_seqs) + 1 #EOS Marker

		#Global variable for device
		self.cuda = device
		'''Build a composer dictionary and a list of songs per composer
		composer_songs = {}
		all_composers = []

		for dir_name in self.findFiles(path="./data/*/"):

			#Get name of composer
			composer_name = os.path.basename(os.path.normpath(dir_name))
			all_composers.append(composer_name)

			#Get number of songs per composer
			num_songs = len(self.findFiles(path=dir_name + "/*.mid"))
			composer_songs[composer_name] = num_songs'''


	#Function that finds tracks with a certain instrument
	def findTracksWithInstrument(self, composer_directory, instrument):

		#Array that is returned in the end
		instrument_tracks = []
		piano_rolls = {}
		#Track name counter for Track names that occur multiple times
		track_name_counter = 0

		#Variable that says to how many decimals a time step is rounded
		round_to_decimals = 1

		#Get all midi files in directory
		composer_files = self.findFiles(composer_directory + "/*.mid")

		#Loop through all songs and filter out piano tracks
		for song in tqdm(composer_files):
			
			#Try and except in case MIDI file is not readable
			try:
				midi_data = MidiFile(song)
			except:
				continue

			#Get ticks per beat
			ticks_per_beat = midi_data.ticks_per_beat


			#Check if Midi Type is 1 as it is the only type that can be used
			if midi_data.type == 1:
				
				#Loop through all tracks
				for i, track in enumerate(midi_data.tracks):

					#Create a 3D-array of notes
					notes = []

					for i in range(self.n_notes):
						notes.append([])

					#Create a bool that decides if the track will be returned
					valid_track = False

					#Variable that keeps track of the time elapsed in song
					cumul_time = 0.0

					for msg in track:

						#Program change = instrument
						#Piano = 0
						#-1 = All instruments allowed
						#Track contains required instrument
						if msg.type == 'program_change':
							if msg.program == instrument:

								#Make track valid
								valid_track = True

						elif instrument == -1:

							#Make track valid
							valid_track = True
						
						#
						#Convert track to tensor
						#
						if msg.type == 'note_on':

							#Calculate delta time and add to cumul time
							delta_time = msg.time/ticks_per_beat
							cumul_time += delta_time

							#Round cumul time to 2 decimals and times it by 10^x
							cumul_time = round(cumul_time, round_to_decimals)

							#Append cumul time in a new array to the note array
							notes[msg.note].append([cumul_time * 10**round_to_decimals])

						elif msg.type == 'note_off':

							#Calculate delta time and add to cumul time
							delta_time = msg.time/ticks_per_beat
							cumul_time += delta_time

							#Round cumul time to 2 decimals ans times it by 10^x
							cumul_time = round(cumul_time, round_to_decimals)

							#Append cumul time to latest array in notes array for note if its length ius greater than 1
							if len(notes[msg.note][-1]) == 1:

								#Append the note off time to the note
								notes[msg.note][-1].append(cumul_time * 10**round_to_decimals)


					#
					#Create a matrix from the midi track (x=time, y=note)
					#
					piano_roll_matrix = np.zeros((math.ceil(cumul_time) * 10**round_to_decimals, self.n_notes))

					#Append piano roll matrix to return list
					for b, note_array in enumerate(notes):
						if len(note_array) > 0:

							for note in note_array:
								if len(note) == 2:

									piano_roll_matrix[int(note[0]):int(note[1]), b] = 1.

					if valid_track and np.size(piano_roll_matrix, axis=0) > 0:

						#Check if key is already used
						if track.name + str(track_name_counter) in piano_rolls:
							track_name_counter += 1

						piano_rolls[track.name + str(track_name_counter)] = piano_roll_matrix

					#
					#From MidiParserSCHM
					#
					linepoints=[]
					for i in range(self.n_notes):
						if len(notes[i]) > 0:
							for note in notes[i]:
								if len(note) == 2:
									linepoints.append([note[0], i, note[1], i])
					#print(linepoints)

					'''for p in linepoints:
						matplotlib.pyplot.plot([p[0],p[2]],[p[1],p[3]])

					matplotlib.pyplot.show()'''

			else:

				#Print error message
				print("Midi file type {} found, 1 required".format(midi_data.type))

		#Return all tracks with instrument in it
		return piano_rolls

	#Test out function
	#print(findTracksWithInstrument("./data/Ghosthack/", -1))
	#Merge all tracks into one??
	#trackToTensor(mido.merge_tracks(findTracksWithInstrument("./data/Arndt/", 0)))

	#Function to fetch a random track from dictionary
	def randomTrack(self, d):
		return d[random.choice(list(d.keys()))]

	#Get unique sequences for dict
	def uniqueSequences(self, track_dict):

		#1. Put all sequencies from category into one array
		all_seqs = np.zeros((1, self.n_notes))
	
		for name, track in tqdm(track_dict.items()):

			all_seqs = np.append(all_seqs, track, axis=0)

		#2. Get unique values from array
		unique_seqs, counts = np.unique(all_seqs, axis=0, return_counts=True)

		#3. Print Occurence of each unique
		print("Occurences:", counts)

		return unique_seqs


	#Get a random one hot unique seq for sampling
	def randomUniqueSequence(self):

		temp = torch.zeros(1, self.n_unique_seqs)
		temp[0][randrange(self.n_unique_seqs)] = 1.

		return temp.to(device=self.cuda)

	#One hot encode piano roll track
	def trackToUniq(self, pr):

		#Array of one hot sequences that is returned in the end
		temp = torch.zeros(len(pr), 1, self.n_unique_seqs)#[]

		for seq in range(len(pr)):#pr:

		    #Find out one-hot index in unique seq list
		    uniq_index = self.unique_seqs.tolist().index(pr[seq].tolist())
		    temp[seq][0][uniq_index] = 1.

		return temp.to(device=self.cuda)

	#Decode one-hot unique sequences to casual midi piano roll
	def uniqToTrack(self, upr, unique_sequences):

		#Array of one hot sequences that is returned in the end
		temp = []

		for seq in upr:

		    #Get Midi pr sequence for unique pr index
		    temp.append(unique_sequences[np.nonzero(seq)[0][1].item()])

		return np.array(temp)

	#Function that turns the composer/category into a one-hot tensor
	def composerTensor(self, composer):
		li = all_composers.index(composer)
		tensor = torch.zeros(1, len(all_composers))
		tensor[0][li] = 1
		return tensor.to(device=self.cuda)

	#Long Tensor from second sequence index to end + EOS tag
	def targetUniq(self, input_uniq):

		uniq_indexes = [np.nonzero(ts)[0][1].item() for ts in input_uniq[1:]]#Skip first entry
		uniq_indexes.append(self.n_unique_seqs - 1)#EOS Marker

		return torch.LongTensor(uniq_indexes).to(device=self.cuda)

	def randomTrainingSet(self):
		track = self.randomTrack(self.pr_data)
		input_track_tensor = self.trackToUniq(track)
		target_track_tensor = self.targetUniq(input_track_tensor)

		return input_track_tensor.to(device=self.cuda), target_track_tensor.to(device=self.cuda)

	#print(randomTrainingSet())
	#targetTensor(randomTrack(findTracksWithInstrument("./data/BrannonDorsey/", -1)))
	#np.set_printoptions(threshold=sys.maxsize)
	#print("Test:", np.array_equal(uniqToTrack(trackToUniq(pr_dict["ACP 4"], unique_seqs), unique_seqs), pr_dict["ACP 4"]))
	#print(np.size(trackToUniq(pr_dict["ACP 4"], unique_seqs), axis=1))
	#print(uniqToTrack(trackToUniq(pr_dict["ACP 4"], unique_seqs), unique_seqs))
	#print(pr_dict["ACP 4"])
	#print(trackToUniq(pr_dict["MIDI Out0"], unique_seqs))
	#print(targetUniq(trackToUniq(pr_dict["ACP 4"], unique_seqs)))
	#print(randomTrainingSet())

    
	#Get item for track index
	def __getitem__(self, index):
        
		#Get track key for index
		track_key = list(self.pr_data.keys())[index]

		#Get track piano roll with key
		track_pr = self.pr_data[track_key]

		#Get input and target UNIQUE tensor
		input_track_tensor = self.trackToUniq(track_pr)
		target_track_tensor = self.targetUniq(input_track_tensor)

		return input_track_tensor.to(device=self.cuda), target_track_tensor.to(device=self.cuda)
        
        
	#Get amount of tracks in dictionmary    
	def __len__(self):
		return len(self.pr_data)
        
		              
